//-------------------------------------------------------
//            Funções e variáveis universais
//-------------------------------------------------------
var tipoColeta; //Valores podem ser "Amostra" ou "População"
var graficoVar = null;

//Medidas Separatrizes
var quintil = [];
var quartil = [];
var decil = [];
var porcentil = [];

function showMedSep(){
    var tipoMedSep = document.getElementById("tipoMedSep").value;
    var quantMedSep = parseInt(document.getElementById("quantMedSep").value);

    switch (tipoMedSep){
        case "q":
            document.getElementById("medSepResult").innerHTML = quartil[quantMedSep - 1];
            break;
        case "k":
            document.getElementById("medSepResult").innerHTML = quintil[quantMedSep - 1];
            break;
        case "d":
            document.getElementById("medSepResult").innerHTML = decil[quantMedSep - 1];
            break;
        case "p":
            document.getElementById("medSepResult").innerHTML = porcentil[quantMedSep - 1];
            break;
    }
}

function prepareCr(){
    if (document.getElementById('dadoEstatistico').value == "Correlação e Regressão") {
        alternaVisibilidade(false, 'naoCr');
        alternaVisibilidade(false, 'naoCr2');
        alternaVisibilidade(false, 'nomeVarLabel');
        alternaVisibilidade(false, 'nomeVar');
        document.getElementById('nomeVar').value = "cr";
        document.getElementById('tipoVar').value = "Correlação e Regressão";
        document.getElementById('tipoVar2').value = "Correlação e Regressão";

    }else{
        alternaVisibilidade(true, 'naoCr');
        alternaVisibilidade(true, 'naoCr2');
        alternaVisibilidade(true, 'nomeVarLabel');
        alternaVisibilidade(true, 'nomeVar');
    }

}

//Entrada e organização de dados
function entrada() {
    var inicial;
    var tipoVar;
    tipoColeta = document.getElementById("dadoEstatistico").value;

    var importado = document.getElementById("dadosImp").value != "";
    if (importado){
        inicial = document.getElementById("dadosImp").value;
        tipoVar = document.getElementById("tipoVar2").value;
    }else{
        inicial = document.getElementById("dados").value;
        tipoVar = document.getElementById("tipoVar").value;
    }
    
    if (inicial == "") {
        alert("Digite pelo menos um número!!!");
        return;
    }
    var nomeVar = document.getElementById("nomeVar").value;
    if (nomeVar == "") {
        alert("Digite um nome para a variavel!!!");
        return;
    }
    
    tipoColeta = document.getElementById("dadoEstatistico").value;

    document.getElementById("nomeVariavel").innerHTML = nomeVar;

    var numeros = inicial.split(';').map(function(item) { return parseFloat(item)});

    var palavras = inicial.split(';');

    var rol = numeros.sort(function (a, b) {  return a - b;  });
    rolPalavras = palavras.sort();


    switch (tipoVar) {
        case "Qualitativa Ordinal":
        case "Qualitativa Nominal":
            qualiNom(rolPalavras);
            break;
        case "Quantitativa Contínua":
            quantCont(rol, nomeVar);
            break;
        case "Quantitativa Discreta":
            quantDis(rol, nomeVar);
            break;
        default:
            calcCorrelacao();
            break;
    }

}


//Desvio padrao
function desvioPadrao (rol){
    var med = 0;
    var soma = 0;

    for(var i = 0; i < rol.length; i++){
        soma = soma + rol[i];
    }

    var media = soma/rol.length;

    for (var i = 0; i < rol.length; i++){
        med += Math.pow((rol[i] - media) ,2);
    }
    var DP = 0;

    if (tipoColeta == "Amostra"){
        DP = med/(rol.length -1);
    }else{
        DP = med/rol.length;
    }
    DP = Math.sqrt(DP);
    return DP.toFixed(2);
}

//Escreve a tabela
function escreveTabela(dados, intervalo){
    var body = document.createElement("tbody");
    var tableRef = document.getElementById('dataTable').getElementsByTagName('tbody')[0];
    tableRef.innerHTML = '';

    for (var i = 0; i < intervalo; i++) {
        var newRow = body.insertRow(body.rows.length);
        for (var j = 0; j < dados.length; j++) {
            var newCell = newRow.insertCell(j);
            var elmt = dados[j];
            var newText = document.createTextNode(elmt[i]);
            newCell.appendChild(newText);
        } 
    }
    tableRef.parentNode.replaceChild(body, tableRef);
}

function alternaVisibilidade(mostrar, elemento){
    var elem = document.getElementById(elemento);
    elem.style.display = mostrar ? "block" : "none";
}

//-------------------------------------------------------
//          Funções para Quantitativa Continua
//-------------------------------------------------------
function quantCont(rol, nomeVar) {
    var numeroClasses = numeroDeClasses(rol);
    var intClasses = intervaloDeClasses(rol);
    var limitesClasse = determinaClasses(rol[0], intClasses, numeroClasses);
    var freqSimples = calculaFrequenciaSimples(rol, limitesClasse, numeroClasses);
    var freqAcumulada = calculaFrequenciaAcumulada(freqSimples);
    var freqSimplesRelatica = calculaFrequenciaSimplesRelativa(freqSimples, rol.length);
    var freqAcumuladaRelativa = calculaFrequenciaAcumuladaRelativa(freqAcumulada, rol.length);



    //Agrupa todos os dados a serem escritos na tabela
    var dados = [formataClasses(limitesClasse), freqSimples, freqSimplesRelatica, freqAcumulada, freqAcumuladaRelativa];

    //Escreve dados na tabela hmtl
    escreveTabela(dados, numeroClasses);


    //Desenha o gráfico
    desenharGraficosContinua(formataClasses(limitesClasse), freqSimplesRelatica, nomeVar);

    var calcMedia = mediaContinua(limitesClasse, freqSimples, rol.length);
    var calcMediana = medianaContinua(rol.length, intClasses, freqSimples, freqAcumulada, limitesClasse);

    calcMedSepCont(rol.length, freqSimples, limitesClasse, freqAcumulada, intClasses);

    alternaVisibilidade(false, 'valoresDiscreta');
    alternaVisibilidade(true, 'valoresContinua');
    alternaVisibilidade(true, 'modasContinua');
    alternaVisibilidade(false, 'valoresQuali');
    alternaVisibilidade(false, 'valoresCorreg');



    var amodal = checaAmodal(freqSimples);
    if(!amodal){
        escreveModa(modaConvencionalContinua(freqSimples, limitesClasse), "modaCList");
        escreveModa(modaCzuber(freqSimples, limitesClasse, intClasses), "modaCzList");
        escreveModa(modaKing(freqSimples, limitesClasse, intClasses), "modaKList");
        document.getElementById("modaP").innerHTML = modaPearson(calcMedia, calcMediana);
    }else{
        alternaVisibilidade(false, 'modasContinua');
    }

    document.getElementById("mediaC").innerHTML = calcMedia;
    document.getElementById("medianaC").innerHTML = calcMediana;
    document.getElementById("desvioPadraoC").innerHTML = desvioPadrao(rol,calcMedia);

}

function intervaloDeClasses(rol){
    var amplitude = rol[rol.length -1] - rol[0];
    amplitude = Math.ceil(amplitude);
    var isDecimal = false;
    if (amplitude % 1 != 0){
        isDecimal = true;
    }

    //Calcula as possiveís classes para divisão
    var sqrtRol = parseInt(Math.sqrt(rol.length));
    var classes = [sqrtRol - 1, sqrtRol, sqrtRol + 1];

    //Determina por qual classe sera feita a divisão
    var numeroClasses;
    var parada = true;
    
    do {
        if (!isDecimal)
        amplitude++;
        for (var j = 0; j < 4; j++) {
            if (amplitude % classes[j] === 0) {
                numeroClasses = classes[j];
                parada = false;
            }

        }
    } while (parada)

    return amplitude / numeroClasses;
}
function numeroDeClasses(rol){

    var amplitude = rol[rol.length -1] - rol[0];

    //Calcula as possiveís classes para divisão
    var sqrtRol = parseInt(Math.sqrt(rol.length));
    var classes = [sqrtRol - 1, sqrtRol, sqrtRol + 1];

    //Determina por qual classe sera feita a divisão
    var numeroClasses;
    var parada = true;
    
    do {
        amplitude++;
        for (var j = 0; j < 4; j++) {
            if (amplitude % classes[j] === 0) {
                numeroClasses = classes[j];
                parada = false;
            }

        }
    } while (parada)

    return numeroClasses;
}

function calcAmplitude(rol){
    return rol[rol.length -1] - rol[0];
}

function determinaClasses(limInferior, intClasses, numeroClasses){
    var limites = [];

    limites.push(limInferior);

    for(var i = 0; i < numeroClasses; i++){
        var limSuperior = limInferior + intClasses;
        limites.push(limSuperior);
        limInferior = limSuperior;
    }

    return limites;

}

function calculaFrequenciaSimples(rol, limitesClasses, numClasses){
    var fi = [];

    for (var i = 0; i < numClasses; i++){
        var limInf = limitesClasses[i];
        var limSup = limitesClasses[i + 1];
        var total = 0;

        for(var j = 0; j < rol.length; j++){
            if(rol[j] >= limInf && rol[j] < limSup){
                total++;
            }
        }
        fi[i] = total;
    } 

    return fi;

}

function calculaFrequenciaAcumulada(freqSimples){
    fAcum = new Array(freqSimples.length).fill(0);

    fAcum[0] = freqSimples[0];

    for(var i = 1; i < freqSimples.length; i++){
        fAcum[i] = fAcum[i-1] + freqSimples[i];
    }

    return fAcum;
}

function calculaFrequenciaSimplesRelativa(fi, numeroElementos){
    var fSimplesRelativa = [];

    for(var i = 0; i < fi.length; i++){
        fSimplesRelativa[i] = ((fi[i] * 100)/numeroElementos).toFixed(2);
    }

    return fSimplesRelativa;
}

function calculaFrequenciaAcumuladaRelativa(freqAcum, numeroElementos){
    var fAcumRelativa = [];

    for(var i = 0; i < freqAcum.length; i++){
        fAcumRelativa[i] = ((freqAcum[i] * 100)/numeroElementos).toFixed(2);
    }

    return fAcumRelativa;
}

function formataClasses(limites){
    var classesTexto = [];
    for(var i = 0; i < limites.length -1; i++){
        classesTexto.push(limites[i] + "\u22a2" + limites[i + 1]);
    }

    return classesTexto;
}

function medianaContinua(numeroElementos, intClasses, freqSimples, freqAcum, limitesClasses){
    var fAcumAnterior;
    var classeMediana;
    var posicaoMediana = numeroElementos /2;

    for(var i = 0; i < freqAcum.length; i++){
        if(freqAcum[i] >= posicaoMediana ){
            classeMediana = i;
            break;
        }
    }

    if(classeMediana == 0){
        fAcumAnterior = 0;
    }else{
        fAcumAnterior = freqAcum[classeMediana - 1];
    }

    var limInferior = limitesClasses[classeMediana];

    var mediana = limInferior + ( (posicaoMediana - fAcumAnterior) / freqSimples[classeMediana]) * intClasses;

    return mediana.toFixed(2);

}

function mediaContinua(limites, fi, numeroElementos){
    var somatorioLimites = 0;
    var mediaLimites = calculaMediaLimites(limites);
    
    for(var i = 0; i < mediaLimites.length; i++){
        somatorioLimites += mediaLimites[i] * fi[i]
    }

    var media = somatorioLimites / numeroElementos;

    return media.toFixed(2);

}

function calculaClasseModal(fi){
    var classes = [];
    var aux = 0;

    for(var i = 0; i < fi.length; i++){
        if(fi[i] > aux){
            aux = fi[i];
        }
    }

    for(var i = 0; i < fi.length; i++){
        if(fi[i] == aux){
            classes.push(i);
        }
    }

    return classes;
}

function calculaMediaLimites(limites){
    var mediaLimites = [];
    for (var i = 0; i < limites.length - 1; i++) {
        var mediaLim = ((limites[i] + limites[i+1])/2).toFixed(2);
        mediaLimites.push(mediaLim);
        
    }

    return mediaLimites;
}

function modaConvencionalContinua(fi, limites){
    var classesModal = calculaClasseModal(fi);
    var medias = calculaMediaLimites(limites);
    var modasConv = [];

    for(var i = 0; i < classesModal.length; i++){
        modasConv.push(medias[classesModal[i]]);    
    }

    return modasConv;
}

function modaCzuber(fi, limites, intClasses){
    var maiorFreq = calculaClasseModal(fi);
    var todasModas = [];
    for(n in maiorFreq){
        var limInferiorClaModal = limites[maiorFreq[n]];
        var fiModal = fi[maiorFreq[n]];
        var fiClaAnterior;
        if(maiorFreq[n] == 0){
            fiClaAnterior = 0;
        }else{
            fiClaAnterior = fi[maiorFreq[n] - 1];
        }
        var fiClaPosterior;
        if(maiorFreq[n] == fi.length -1){
            fiClaPosterior = 0;
        }else{
            fiClaPosterior = fi[maiorFreq[n] + 1];
        }

        var moda = limInferiorClaModal +  (((fiModal - fiClaAnterior) / ((fiModal - fiClaAnterior) + (fiModal - fiClaPosterior))) * intClasses);
        todasModas.push(moda.toFixed(2));
    }

    return todasModas;
}

function modaKing(fi, limites, intClasses){
    var maiorFreq = calculaClasseModal(fi);
    var todasModas = [];
    for(n in maiorFreq){
        var limInferiorClaModal = limites[maiorFreq[n]];
        var fiClaAnterior;
        if(maiorFreq[n] == 0){
            fiClaAnterior = 0;
        }else{
            fiClaAnterior = fi[maiorFreq[n] - 1];
        }
        var fiClaPosterior;
        if(maiorFreq[n] == fi.length -1){
            fiClaPosterior = 0;
        }else{
            fiClaPosterior = fi[maiorFreq[n] + 1];
        }
        var passo1 = fiClaPosterior / (fiClaPosterior - fiClaAnterior);
        var passo2 = passo1 * intClasses;
        var passo3 = passo2 + limInferiorClaModal;

        var moda = limInferiorClaModal + (fiClaPosterior / (fiClaPosterior + fiClaAnterior)) * intClasses;
        todasModas.push(moda.toFixed(2));
    }

    return todasModas;
}

function modaPearson(media, mediana){
    var moda = (3 * mediana) - ( 2 * media);
    return moda.toFixed(2);
}

//Plota o graficoVar
function desenharGraficosContinua(limites, fi, nomeVar){
    var ctx = document.getElementById("myChart").getContext('2d');
    if(graficoVar != null){
        graficoVar.destroy();
    }

    graficoVar = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: limites,
        datasets: [
            {
                label: "fr%",
                data: fi,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(245, 0, 0, 0.2)',
                    'rgba(0, 122, 245, 0.2)',
                    'rgba(234, 255, 5, 0.2)',
                    'rgba(255, 97, 5, 0.2)',
                    'rgba(5, 255, 138, 0.2)',
                    'rgba(126, 5, 255, 0.2)',
                    'rgba(255, 5, 5, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(245, 0, 0, 1)',
                    'rgba(0, 122, 245, 1)',
                    'rgba(234, 255, 5, 1)',
                    'rgba(255, 97, 5, 1)',
                    'rgba(5, 255, 138, 1)',
                    'rgba(126, 5, 255, 1)',
                    'rgba(255, 5, 5, 1)'
                ],
                borderWidth: 1
            }
        ]
    },
        options: {
            legend: { display: false },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }],
                xAxes: [{
                    categoryPercentage: 1.0,
                    barPercentage: 1.0
                }]
            },
            title: {
                display: true,
                text: nomeVar
            }
        }
    });
}

function calcMedSepCont(numeroElementos, fi, limites, fAc, intClasses){
    var porcentagem = 0;
    var posicao = 0;

    //Quartil
    for(var i = 1; i <= 4; i++){
        porcentagem = 0.25 * i;
        posicao = (numeroElementos * porcentagem).toFixed(0);
        
        var classe = 0;
        for(var j = 0; j < fAc.length; j++){
            if(posicao < fAc[j]){
                classe = j;
                break;
            }
        }

        var limInferior = limites[classe];

        var fAnt;
        if(classe == 0){
            fAnt = 0;
        }else{
            fAnt = fAc[classe - 1];
        }

        quartil[i-1] = limInferior + ((posicao - fAnt)/fi[classe]) * intClasses;
    }

     //Quintil
     for(var i = 1; i <= 5; i++){
        porcentagem = 0.2 * i;
        posicao = (numeroElementos * porcentagem).toFixed(0);
        var classe = 0;
        for(var j = 0; j < fAc.length; j++){
            if(posicao < fAc[j]){
                classe = j;
                break;
            }
        }

        var limInferior = limites[classe];

        var fAnt;
        if(classe == 0){
            fAnt = 0;
        }else{
            fAnt = fAc[classe - 1];
        }

        quintil[i-1] = limInferior + ((posicao - fAnt)/fi[classe]) * intClasses;
    }

     //Decil
     for(var i = 1; i <= 10; i++){
        porcentagem = 0.1 * i;
        posicao = (numeroElementos * porcentagem).toFixed(0);
        var classe = 0;
        for(var j = 0; j < fAc.length; j++){
            if(posicao < fAc[j]){
                classe = j;
                break;
            }
        }

        var limInferior = limites[classe];

        var fAnt;
        if(classe == 0){
            fAnt = 0;
        }else{
            fAnt = fAc[classe - 1];
        }

        decil[i-1] = limInferior + ((posicao - fAnt)/fi[classe]) * intClasses;
    }

     //Porcentil
     for(var i = 1; i <= 100; i++){
        porcentagem = 0.01 * i;
        posicao = (numeroElementos * porcentagem).toFixed(0);
        var classe = 0;
        for(var j = 0; j < fAc.length; j++){
            if(posicao < fAc[j]){
                classe = j;
                break;
            }
        }

        var limInferior = limites[classe];

        var fAnt;
        if(classe == 0){
            fAnt = 0;
        }else{
            fAnt = fAc[classe - 1];
        }

        porcentil[i-1] = limInferior + ((posicao - fAnt)/fi[classe]) * intClasses;
    }
}

function escreveModa(modas, elemento){
    var ul = document.getElementById(elemento);
    ul.innerHTML = "";

    for(n in modas){
        var li = document.createElement('li');
        li.innerHTML = modas[n]; 
        ul.appendChild(li); 
    }
}

function checaAmodal(array){
    return !array.some(function(value, index, array){
        return value !== array[0];
    });
}

//-------------------------------------------------------
//          Funções para Quantitativa Discreta
//-------------------------------------------------------

//var vet = [1,1,2,2,3,3,4,4];
function quantDis(rol, nomeVar) {
    //Encontra todas as classes
    var cla = [];
    for (var i = 0; i < rol.length; i++) {
        if (!cla.includes(rol[i])) {
            cla.push(rol[i]);
        }
    }

    //Calcula fi
    var fi = new Array(cla.length).fill(0);
    for (var i = 0; i < rol.length; i++) {
        for (var j = 0; j < cla.length; j++) {
            if (rol[i] == cla[j]) {
                fi[j] = fi[j] + 1;

            }
        }
    }
    
    //Calcula f%
    var fr = new Array(fi.length);
    for (var i = 0; i < fi.length; i++) {
        fr[i] = ((fi[i] * 100) / rol.length).toFixed(2);
    }

    //Calcula F
    var num = 0;
    var Fx = new Array();
    for (var i = 0; i < fi.length; i++) {
        Fx.push(num + fi[i]);
        num = num + fi[i];
    }

    //Calcula F%
    var Fp = new Array(fr.length);
    for (var i = 0; i < fr.length; i++) {
        Fp[i] = ((Fx[i] * 100) / rol.length).toFixed(2);
    }

    //Calcula mediana
    var mediana;
    if (rol.length % 2 == 0) {
        var posicao= (rol.length)/2;
        mediana = (rol[posicao-1] + rol[posicao]) / 2;
        console.log(rol[posicao]);
    } else {
        var posicao = (rol.length+1)/2;
        mediana = rol[posicao-1];
    }

    //Calcula desvio padrão
    var mediaA = mediaDiscreta(rol);
    var dp;
    var div;
    var soma = 0;
    for (i in fi) {
        var result = (cla[i] - mediaA);
        var result2 = Math.pow(result, 2) * fi[i];
        soma = soma + result2;
        }

    div = soma / (rol.length - 1);
    dp = Math.sqrt(div).toFixed(2);

    alternaVisibilidade(false, 'valoresContinua');
    alternaVisibilidade(false, 'modasContinua');
    alternaVisibilidade(true, 'valoresDiscreta');
    alternaVisibilidade(false, 'valoresQuali');
    alternaVisibilidade(false, 'valoresCorreg');

    
    document.getElementById("desvioPadraoDiscreta").innerHTML = desvioPadrao(rol, mediaDiscreta(rol));
    document.getElementById("mediaDisc").innerHTML = mediaDiscreta(rol);
    document.getElementById("mediana").innerHTML = mediana;
    document.getElementById("moda").innerHTML = modaDiscreta(fi, cla);

    //Agrupa dados e os escreve na tabela
    var dados = [cla, fi, fr, Fx, Fp];
    escreveTabela(dados, cla.length);
    desenhaGraficoDiscreta(cla, fr, nomeVar);

    calcMedSepDisc(rol);
}

function calcMedSepDisc(rol){
    var porcentagem = 0;
    var posicao = 0;

    //Quartil
    for(var i = 1; i <= 4; i++){
        porcentagem = 0.25 * i;
        posicao = (rol.length * porcentagem).toFixed(0);
        quartil[i-1] = medida = rol[posicao -1];
    }

     //Quintil
     for(var i = 1; i <= 5; i++){
        porcentagem = 0.2 * i;
        posicao = (rol.length * porcentagem).toFixed(0);
        quintil[i-1] = medida = rol[posicao -1];
    }

     //Decil
     for(var i = 1; i <= 10; i++){
        porcentagem = 0.1 * i;
        posicao = (rol.length * porcentagem).toFixed(0);
        decil[i-1] = medida = rol[posicao -1];
    }

     //Porcentil
     for(var i = 1; i <= 100; i++){
        porcentagem = 0.01 * i;
        posicao = (rol.length * porcentagem).toFixed(0);
        porcentil[i-1] = medida = rol[posicao -1];
    }
}

// function medidaSeparatrizDiscreta(){
//     var tipo = document.getElementById("tipoMedSep").value;
//     var numero = document.getElementById("quantMedSep").value;

//     var porcentagem = 0;
//     var posicao = 0;
//     var medida;

//     if(tipo == "Q"){
//         porcentagem = 0.25 * numero;
//         posicao = (rol.length * porcentagem).toFixed(0);
//         medida = rol[posicao -1];
//     }else if (tipo == "K"){
//         porcentagem = 0.2 * numero;
//         posicao = (rol.length * porcentagem).toFixed(0);
//         medida = rol[posicao -1];    
//     }else if(tipo == "D"){
//         porcentagem = 0.1 * numero;
//         posicao = (rol.length * porcentagem).toFixed(0);
//         medida = rol[posicao -1];
//     }else{
//         var porcentagem = 0.01 * numero;
//         var posicao = (rol.length * porcentagem).toFixed(0);
//         medida = rol[posicao -1];
//     }
//     alert(medida);
// }

//Obter moda convencional (Discreta)
function modaDiscreta(fi, cla) {
    var maiorFreq = fi[0];
    var modaConven = cla[0];

    for (i in fi) {
        if (fi[i] >= maiorFreq) {
            modaConven = cla[i];
            maiorFreq = fi[i]
        }
    }

    var moda = [];
    for (i in fi) {
        if (maiorFreq == fi[i]) {
            moda.push(cla[i]);
        }
    }

    if(moda.length == cla.length){
        moda = "Amodal";
    }

    return moda;
}

//Calcula media para variavel Quantitativa Discreta
function mediaDiscreta(rol) {
    var soma = 0;
    for (var i = 0; i < rol.length; i++) {
        soma = soma + rol[i];
    }

    var mediaDisc = soma / rol.length;

    return mediaDisc.toFixed(2);
}

function desenhaGraficoDiscreta(cla, fr){
    var ctx = document.getElementById("myChart").getContext('2d');
    if(graficoVar != null){
        graficoVar.destroy();
    }
    graficoVar = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: cla,
        datasets: [
            {
                label: "fr%",
                data: fr,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(245, 0, 0, 0.2)',
                    'rgba(0, 122, 245, 0.2)',
                    'rgba(234, 255, 5, 0.2)',
                    'rgba(255, 97, 5, 0.2)',
                    'rgba(5, 255, 138, 0.2)',
                    'rgba(126, 5, 255, 0.2)',
                    'rgba(255, 5, 5, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(245, 0, 0, 1)',
                    'rgba(0, 122, 245, 1)',
                    'rgba(234, 255, 5, 1)',
                    'rgba(255, 97, 5, 1)',
                    'rgba(5, 255, 138, 1)',
                    'rgba(126, 5, 255, 1)',
                    'rgba(255, 5, 5, 1)'
                ],
                borderWidth: 1
            }
        ]
    },
    });
}


//-------------------------------------------------------
//          Funções para Qualitativa Nominal e Ordinal
//-------------------------------------------------------

function qualiNom(rol) {
    var classes = [];
    for (var i = 0; i < rol.length; i++) {
        if (!classes.includes(rol[i])) {
            classes.push(rol[i]);
        }
    }


    //Calcula fi
    var fi = new Array(classes.length).fill(0);
    for (var i = 0; i < rol.length; i++) {
        for (var j = 0; j < classes.length; j++) {
            if (rol[i] == classes[j]) {
                fi[j] = fi[j] + 1
            }
        }
    }


    //Calcula f%
    var freq = new Array(fi.length);
    for (var i = 0; i < fi.length; i++) {
        freq[i] = ((fi[i] * 100) / rol.length).toFixed(2);
    }


    //Calcula F
    var num = 0;
    var freqA = new Array();
    for (var i = 0; i < fi.length; i++) {
        freqA.push(num + fi[i]);
        num = num + fi[i];
    }
  

    //Calcula F%
    var FreqP = new Array(freq.length);
    for (var i = 0; i < freq.length; i++) {
        FreqP[i] = ((freqA[i] * 100) / rol.length).toFixed(2);
    }

    alternaVisibilidade(false, 'valoresContinua');
    alternaVisibilidade(false, 'modasContinua');
    alternaVisibilidade(false, 'valoresDiscreta');
    alternaVisibilidade(false, 'valoresCorreg');

    gerarTabela(classes,fi,freq,freqA,FreqP);
    desenhaGraficoDiscreta(classes, freq);

    modaNominalOrdinal(fi, classes);
}


    //Obter moda convencional (Qualitativa nominal e ordinal)
function modaNominalOrdinal(fi, classes) {
    var maiorFreq = fi[0];
    var modaConven = classes[0];

    for (i in fi) {
        if (fi[i] >= maiorFreq) {
            modaConven = classes[i];
            maiorFreq = fi[i]
        }
    }

    var moda = [];
    for (i in fi) {
        if (maiorFreq == fi[i]) {
            moda.push(classes[i]);
        }
    }

    if(moda.length == classes.length){
        moda = "Amodal";
    }

    alternaVisibilidade(false, 'valoresContinua');
    alternaVisibilidade(false, 'modasContinua');
    alternaVisibilidade(false, 'valoresDiscreta');
    alternaVisibilidade(true,"valoresQuali");

    document.getElementById("modaQuali").innerHTML = moda;

}

//Gera tabela Qualitativas

function gerarTabela(classes,fi,freq,freqA,FreqP) {
    var tableRef = document.getElementById('dataTable').getElementsByTagName('tbody')[0];
    tableRef.innerHTML = '';
    
    for(var i = 0; i < classes.length; i++) {
        var newRow = tableRef.insertRow(tableRef.rows.length);
        gerarColuna(newRow, 0, classes[i]);
        gerarColuna(newRow, 1, fi[i]);
        gerarColuna(newRow, 2, freq[i]);
        gerarColuna(newRow, 3, freqA[i]);
        gerarColuna(newRow, 4, FreqP[i]);    
    }

}

function gerarColuna(row, index, value) {
    var newCell = row.insertCell(index);
    var newText = document.createTextNode(value);
    newCell.appendChild(newText);
}

function qualOrdinal(rol){
    var rol
}
