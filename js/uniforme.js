
function uniforme (){
    var a = parseFloat(document.getElementById("a").value);
    var b = parseFloat(document.getElementById("b").value);

    var maiorQue = parseFloat(document.getElementById("maiorQueUN").value);
	var menorQue = parseFloat(document.getElementById("menorQueUN").value);
    var entre = document.getElementById("interUN").value.split(";").map(function(item) { return parseFloat(item)});
    var int;

	if (!isNaN(maiorQue)){
        int = b - maiorQue;
	}else if(!isNaN(menorQue)){
		int = menorQue - a;
	}else if(entre.length == 2){
        int = entre[1] - entre[0];    
    }

    var media = (b + a)/2;

    var dp = Math.sqrt(Math.pow(b-a, 2)/12);

    var cv =(dp/media)*100;

    var probDu = (1/(b-a)) * int;

    document.getElementById("probDu").innerHTML = arredondar(probDu) +" %";
	document.getElementById("media").innerHTML =arredondar(media);
	document.getElementById("coefVar").innerHTML = arredondar(cv);
    document.getElementById("dPadrao").innerHTML = arredondar(dp);   

}

function arredondar(numero){
    return numero.toFixed(2).replace(/\.00$/, '');
}
